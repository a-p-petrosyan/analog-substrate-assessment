// use crate::{mock::*, Error, Event};
// use frame_support::{assert_noop, assert_ok};
use crate::{Every, RenewalPeriod};
use core::num::{NonZeroU16, NonZeroU8};

#[test]
fn daily_expenses() {
	let cycle = RenewalPeriod::Daily(Every(NonZeroU16::new(2).unwrap()));
	assert_eq!(cycle.expense_per_cycle(365), 2);
	assert_eq!(cycle.expense_per_cycle(1000), 6, "3*365 = 1095 >= annual_expenses");
	let almost_year = RenewalPeriod::Daily(Every(NonZeroU16::new(364).unwrap()));
	assert_eq!(almost_year.expense_per_cycle(1), 1);
	assert_eq!(almost_year.expense_per_cycle(2), 2);
	let more_than_year = RenewalPeriod::Daily(Every(NonZeroU16::new(366).unwrap()));
	// TODO: cover with Proptest?
	assert_eq!(more_than_year.expense_per_cycle(1), 2);
	assert_eq!(more_than_year.expense_per_cycle(3), 4);
}

#[test]
fn monthly_expenses() {
	let cycle = RenewalPeriod::Monthly(Every(NonZeroU8::new(2).unwrap()));
	assert_eq!(cycle.expense_per_cycle(12), 2);
	assert_eq!(cycle.expense_per_cycle(25), 5, "Round up to 30");
	let almost_year = RenewalPeriod::Monthly(Every(NonZeroU8::new(11).unwrap()));
	assert_eq!(almost_year.expense_per_cycle(1), 1);
	assert_eq!(almost_year.expense_per_cycle(2), 2);
	let more_than_year = RenewalPeriod::Monthly(Every(NonZeroU8::new(13).unwrap()));
	// TODO: cover with Proptest?
	assert_eq!(more_than_year.expense_per_cycle(1), 2);
	assert_eq!(more_than_year.expense_per_cycle(3), 4);
}

// #[test]
// fn it_works_for_default_value() {
// 	new_test_ext().execute_with(|| {
// 		// Go past genesis block so events get deposited
// 		System::set_block_number(1);
// 		// Dispatch a signed extrinsic.
// 		assert_ok!(TemplateModule::do_something(RuntimeOrigin::signed(1), 42));
// 		// Read pallet storage and assert an expected result.
// 		assert_eq!(TemplateModule::something(), Some(42));
// 		// Assert that the correct event was deposited
// 		System::assert_last_event(Event::SomethingStored { something: 42, who: 1 }.into());
// 	});
// }

// #[test]
// fn correct_error_for_none_value() {
// 	new_test_ext().execute_with(|| {
// 		// Ensure the expected error is thrown when no value is present.
// 		assert_noop!(
// 			TemplateModule::cause_error(RuntimeOrigin::signed(1)),
// 			Error::<Test>::NoneValue
// 		);
// 	});
// }
