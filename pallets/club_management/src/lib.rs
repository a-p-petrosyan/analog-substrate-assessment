#![cfg_attr(not(feature = "std"), no_std)]
use core::num::{NonZeroU16, NonZeroU8};

/// Edit this file to define custom logic or remove it if it is not needed.
/// Learn more about FRAME and the core library of Substrate FRAME pallets:
/// <https://docs.substrate.io/reference/frame-pallets/>
pub use club_pallet::*;
use codec::{Decode, Encode, MaxEncodedLen};
use scale_info::TypeInfo;

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;

#[derive(
	Debug, Clone, Copy, Encode, Decode, MaxEncodedLen, TypeInfo, PartialEq, Eq, PartialOrd, Ord,
)]
#[repr(transparent)]
pub struct Every<T>(pub T);

impl<T> Every<T> {
	pub fn new(value: impl Into<T>) -> Self {
		Self(value.into())
	}
}

// TODO: use Config::MAX_YEARS
/// Renewal Period for each club subscription
#[derive(
	Debug, Clone, Copy, Encode, Decode, MaxEncodedLen, TypeInfo, PartialEq, Eq, PartialOrd, Ord,
)]
pub enum RenewalPeriod {
	/// Renew every year for at most
	/// [`crate::club_pallet::Config::MAX_YEARS`], which is `u8`
	Annually(Every<NonZeroU8>),
	/// Renew every n-th month. More than 12 at a time is
	/// considered an error, use [`Self::Annually`] instead.
	Monthly(Every<NonZeroU8>),
	/// Renew every n-th week. More than 4 is considered an error,
	/// use [`Self::Monthly`] instead.
	Weekly(Every<NonZeroU8>),
	/// Renew daily. More than 36, 525 (leap years) exceeds 100
	/// years, and is considered an error. Use in cases where high
	/// level granularity is required.
	Daily(Every<NonZeroU16>),
}

impl RenewalPeriod {
	/// Construct [`Self::Daily`]
	pub fn daily(value: impl Into<u16>) -> Option<Self> {
		let v = value.into();
		(v > 0 && v < 36525)
			.then_some(Self::Daily(Every(NonZeroU16::new(v).expect("Already checked"))))
	}

	/// Construct [`Self::Monthly`]
	pub fn monthly(value: impl Into<u8>) -> Option<Self> {
		let v = value.into();
		(v > 0 && v < 12)
			.then_some(Self::Monthly(Every(NonZeroU8::new(v).expect("Already checked"))))
	}

	/// Construct [`Self::Weekly`]
	pub fn weekly(value: impl Into<u8>) -> Option<Self> {
		let v = value.into();
		(v > 0 && v < 4).then_some(Self::Weekly(Every(NonZeroU8::new(v).expect("Already checked"))))
	}

	/// Construct [`Self::Annually`]
	pub fn annually(value: impl Into<u8>) -> Option<Self> {
		let v = value.into();
		(v > 0 && v < 100)
			.then_some(Self::Monthly(Every(NonZeroU8::new(v).expect("Already checked"))))
	}

	pub fn expense_per_cycle(self, annual_expenses: u64) -> u64 {
		// We want to over-estimate the subscription fee, so that no
		// matter what, the club is not losing money. Hence, both
		// `saturating_div_ceil`, and estimating 52 weeks a year (not 53)
		match self {
			RenewalPeriod::Annually(years) => u64::from(u8::from(years.0)) * annual_expenses,
			RenewalPeriod::Monthly(months) =>
				saturating_div_ceil(u64::from(u8::from(months.0)) * annual_expenses, 12),
			RenewalPeriod::Weekly(weeks) =>
				saturating_div_ceil(u64::from(u8::from(weeks.0)) * annual_expenses, 52),
			RenewalPeriod::Daily(days) => {
				// Or could just use `#![feature(int_roundings)]` when that's stable
				saturating_div_ceil(u64::from(u16::from(days.0)) * annual_expenses, 365)
			},
		}
	}

	pub fn years_per_cycle(self) -> u64 {
		match self {
			RenewalPeriod::Annually(years) => u64::from(u8::from(years.0)),
			RenewalPeriod::Monthly(months) => 12 * u64::from(u8::from(months.0)),
			RenewalPeriod::Weekly(weeks) => 52 * u64::from(u8::from(weeks.0)),
			RenewalPeriod::Daily(days) => 365 * u64::from(u16::from(days.0)),
		}
	}
}

pub fn saturating_div_ceil(numerator: u64, denominator: u64) -> u64 {
	if denominator == 0 {
		u64::MAX
	} else {
		(numerator / denominator) + (numerator % denominator != 0) as u64
	}
}

// TODO #1: Store club owners on-chain.

// TODO #2: Create club operation,
// by root and not club owner. Filter?

// TODO #3: Club owner transfer operation,
// per owner. Root involved?

// TODO #4: Club owner can add new users

// TODO #5: Store per-club annual expenses.

// TODO #6: Account pays amount

// TODO #7: Set up withdrawal for each club billing period individually

// TODO #8: Move users with zero subscription cycles into pending renewal.

// TODO #9: Renew callable, which accepts user and token, transfers
// that token to owner, and grants credit.

// TODO #10: Weights

// TODO #11: Benchmarks

#[cfg(feature = "runtime-benchmarks")]
mod benchmarking;

#[frame_support::pallet]
pub mod club_pallet {
	use crate::RenewalPeriod;
	use codec::{alloc::collections::BTreeMap, Decode, Encode};
	use frame_support::{
		pallet_prelude::*,
		sp_runtime::{
			traits::CheckedAdd, ArithmeticError, DispatchError, SaturatedConversion, Saturating,
		},
		storage::bounded_btree_map::BoundedBTreeMap,
	};
	use frame_system::{ensure_root, pallet_prelude::*};
	use scale_info::TypeInfo;

	type ClubId = u32;

	/// Type representing a `Club`
	#[derive(Clone, Encode, Decode, MaxEncodedLen, TypeInfo, PartialEq, Eq)]
	#[scale_info(skip_type_params(T))]
	pub struct Club<T: Config> {
		/// Mapping from user name to number of billing cycles that
		/// the user has managed to purchase. When user transfers X
		/// amount of [`Token`], we immediately calculate how many
		/// billing cycles this allows them to buy a subscription for,
		/// and count down to zero. Because the payment is rarely
		/// going to be an integer amount of cycles, and refunding
		/// might be expensive, we also store a credit towards the
		/// future subxcriptions.
		// FIXME: `Config::ActiveUsersCount` breaks `Clone`
		users: BoundedBTreeMap<T::AccountId, (u32, BalanceOf<T>), ConstU32<4096>>,

		/// Billing cycle to use for this club.
		billing_cycle: RenewalPeriod,

		/// Expenses total for [`Self`] per Annum.
		annual_expenses: BalanceOf<T>,

		/// Users whose subscriptions have expired, but who can,
		/// nonetheless renew their subscription. We don't remove
		/// people from the club entirely so that the user can renew their subscription.
		// FIXME: `Config::ActiveUsersCount` breaks `Clone`
		users_pending_renewal: BoundedBTreeMap<T::AccountId, BalanceOf<T>, ConstU32<4096>>,
	}

	impl<T: Config> core::fmt::Debug for Club<T> {
		fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
			f.debug_struct("Club")
				.field("users", &self.users)
				.field("billing_cycle", &self.billing_cycle)
				.field("annual_expenses", &self.annual_expenses)
				.finish()
		}
	}

	impl<T: Config> Club<T> {
		/// Compute the amount of whole billing cycles assured by
		/// provided payment and return it + the remainder of the
		/// [`T::Token`] that cannot be converted into a whole billing
		/// cycle. It counts as a credit for the next subscription
		/// renewal.
		pub fn cycles(&self, amount: BalanceOf<T>) -> (u32, BalanceOf<T>) {
			// QUESTION: rewrite using `fixed::FixedPoint`?
			let annual_expenses: u64 = self.annual_expenses.saturated_into();
			let expenses_per_user: u64 = annual_expenses / (self.users.len() as u64);
			let expenses_per_billing_cycle: u64 =
				self.billing_cycle.expense_per_cycle(expenses_per_user);
			let subscription_duration = u64::max(
				(amount).saturated_into::<u64>() / expenses_per_billing_cycle,
				T::MaxYears::get() as u64 / self.billing_cycle.years_per_cycle(),
			);
			let credit = amount.saturating_sub(subscription_duration.saturated_into());
			(
				subscription_duration
					.try_into()
					.expect("Overflow in subscription duration is a logic error"),
				credit,
			)
		}
	}

	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	/// Club-related event master enum. Events are emitted when
	/// creating a club, changing club owners, changing the annual
	/// expenses of a club,
	pub enum Event<T: Config> {
		/// A club was created
		ClubCreated { club_id: ClubId, owner: Option<T::AccountId> },

		/// A Club owner has changed
		ClubOwnerChanged {
			club_id: ClubId,
			old_owner: Option<T::AccountId>,
			new_owner: T::AccountId,
		},

		/// A user was added to `club_id`
		UserAdded { club_id: ClubId, added_user: T::AccountId },

		/// Annual expenses changed
		AnnualExpensesChanged { club_id: ClubId, annual_expenses: BalanceOf<T> },

		/// Subscription expired
		SubscriptionExpired { club_id: ClubId, for_whom: T::AccountId },

		/// Renewal was attempted
		RenewalAttempted { outcome: Renewal<T>, for_whom: T::AccountId },
	}

	#[derive(Encode, Decode, Clone, TypeInfo, PartialEq, Eq)]
	/// Renewal Status.
	pub enum Renewal<T: Config> {
		/// Subscription has been successfully renewed and `cycles` have been added to the user.
		Succeeded { cycles: u32, credit: BalanceOf<T> },

		/// Subscription renewal failed, but rather than give the user
		/// a refund their new credit towards a new subscription is
		/// updated.
		Failed { credit: BalanceOf<T> },
	}

	impl<T: Config> core::fmt::Debug for Renewal<T>
	where
		BalanceOf<T>: core::fmt::Debug,
	{
		fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
			match self {
				Self::Succeeded { cycles, credit } =>
					write!(f, "Succeeded {{ cycles: {cycles}, credit: {credit:?} }}"),
				Self::Failed { credit } => write!(f, "Failed {{ credit: {credit:?} }}"),
			}
		}
	}

	#[pallet::pallet]
	pub struct Pallet<T>(_);

	/// Configure the pallet by specifying the parameters and types on
	/// which it depends.
	#[pallet::config]
	pub trait Config:
		frame_system::Config + pallet_scheduler::Config + pallet_timestamp::Config
	{
		#[pallet::constant]
		/// Max number (in years) of subscription renewal
		type MaxYears: Get<u8>; // = 100

		/// The currency token that is used to buy membership for X
		/// amount of billing cycles. Note this pallet doesn't
		/// actually implement transferring of currencies.
		type Token: frame_support::traits::Currency<Self::AccountId> + Clone + core::fmt::Debug;

		/// Runtime event (automatically generated by pallet macros)
		type RuntimeEvent: From<Event<Self>> + IsType<<Self as frame_system::Config>::RuntimeEvent>;

		#[pallet::constant]
		type ActiveUsersCount: Get<u32>;

		#[pallet::constant]
		type UsersPendingRenewalCount: Get<u32>;

		// TODO: I guess things changed since the last time I used
		// substrate pallets. Might as well do it with the new
		// pallets.

		// type ScheduledCall: Parameter
		// 	+ frame_support::dispatch::Dispatchable<Origin = Self::Origin>
		// 	+ From<Call<Self>>;

		// type Scheduler: ScheduleNamed<Self::BlockNumber, Self::ScheduledCall, Self::Origin>;
	}

	/// Type representing a balance of a transaction. Usually reserved
	/// for making transactions. Useful for working out how much money
	/// needs to be exchanged `(T::Token)` in order to purchase a set
	/// amount of subscription billing cycles.
	pub type BalanceOf<T> = <<T as Config>::Token as frame_support::traits::Currency<
		<T as frame_system::Config>::AccountId,
	>>::Balance;

	/// Club owners.
	/// We store them separately, for two reasons:
	///
	/// 1. Only root can reassign ownership. Club owners should be
	/// able to change the club itself, but not reassign ownership
	/// (this can lead to subtle problems with security, where a
	/// trustworthy front-account is used and ownership is immediately
	/// transferred to another non-trustworthy account).
	///
	/// 2. Ownership change is a simple lightweight
	/// operation. Changing club data might potentially be a lot more
	/// involved. Decoupling the two should be beneficial for
	/// performance in the average case.
	#[pallet::storage]
	pub(crate) type ClubOwners<T: Config> =
		StorageMap<_, Twox64Concat, ClubId, Option<T::AccountId>, ValueQuery>;

	#[pallet::storage]
	/// A value holding the id of the last club.
	///
	/// # Performance
	///
	/// This might seem
	/// inefficient, given that it's a database access, however, given
	/// that it allows us to not instantiate the entire set of clubs
	/// whenever we need to query for the last club id, this reduces
	/// the expense needed to hash (and therefore decode) other
	/// database operations.
	pub(crate) type LastClubId<T: Config> = StorageValue<_, ClubId, ValueQuery>;

	/// Clubs themselves
	#[pallet::storage]
	pub(crate) type Clubs<T: Config> =
		StorageMap<_, Twox64Concat, ClubId, Option<Club<T>>, ValueQuery>;

	#[pallet::error]
	/// Master error enum used in this pallet.
	pub enum Error<T> {
		/// There are too many clubs and a club must be closed.
		TooManyClubs,

		/// Two clubs with the same name exist
		ClubNameCollision,

		/// A club with this name doesn't exist
		NonExistentClub,

		/// This club has no owner
		NonExistentOwner,

		/// The current `origin` isn't allowed to execute this operation.
		Unauthorised,

		/// The same user had been added twice
		UserCollision,

		/// There are too many accounts pending renewal. In a real
		/// blockchain we'd ask if we want to eject some old users,
		/// but we need to figure out which users are considered old,
		/// plus we keep track of the credit.
		TooManyAccountsPendingRenewal,

		/// There are too many active users.
		TooManyActiveUsers,
	}

	/// Helper function
	fn renew_subscription<T: Config>(
		club: &mut Club<T>,
		account: &T::AccountId,
		payment: BalanceOf<T>,
	) -> Result<(), DispatchError> {
		if let Some(credit) = club.users_pending_renewal.get(account) {
			let cycles = club.cycles(payment.checked_add(credit).ok_or(ArithmeticError::Overflow)?);

			if cycles.0 == 0 {
				*club.users_pending_renewal.get_mut(account).unwrap() = cycles.1;
				Ok(())
			} else {
				club.users_pending_renewal.remove(account);
				club.users
					.try_insert(account.clone(), cycles)
					.map_err(|_| Error::<T>::TooManyActiveUsers)?;
				Ok(())
			}
		} else if let Some((residual_cycles, residual_credit)) = club.users.get(account) {
			let new_cycles =
				club.cycles(payment.checked_add(residual_credit).ok_or(ArithmeticError::Overflow)?);
			let new_cycles = (
				residual_cycles.checked_add(&new_cycles.0).ok_or(ArithmeticError::Overflow)?,
				new_cycles.1,
			);
			*club.users.get_mut(account).unwrap() = new_cycles;
			Ok(())
		} else {
			return Err(ArithmeticError::Overflow.into())
		}
	}

	#[pallet::call]
	impl<T: Config> Pallet<T> {
		#[pallet::call_index(0)]
		#[pallet::weight(T::DbWeight::get().reads_writes(3, 2) + Weight::from(10_000))]
		/// Add new club.
		///
		/// # Errors
		/// - Forwards [`Self::add_club`] errors
		/// - Overflow
		///
		/// # Panics
		/// - If Adding a club failed silently
		pub fn add_new_club(
			origin: OriginFor<T>,
			club: Club<T>,
			owner: Option<T::AccountId>,
		) -> DispatchResult {
			ensure_root(origin)?;
			let last_club_id = LastClubId::<T>::get();
			let new_last_club_id = last_club_id.checked_add(1).ok_or(ArithmeticError::Overflow)?;
			// Here we're assuming that it's impossible to have an entry behind `new_last_club_id`.
			Self::add_club(new_last_club_id, club)?;
			LastClubId::<T>::set(new_last_club_id);
			if let Some(ref owner) = owner {
				Self::set_club_owner(owner.clone(), new_last_club_id)
					.expect("Adding club failed silently");
			}
			Self::deposit_event(Event::ClubCreated { owner, club_id: LastClubId::<T>::get() });
			Ok(())
		}

		#[pallet::call_index(1)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1) + Weight::from(7_000))]
		/// Transfer club ownership from one user to another.
		///
		/// Because adding users is reserved to root, this operation
		/// if allowed to any user other than root can potentially
		/// defeat that restriction. As such it is privileged and must
		/// be run by `root`.
		pub fn transfer_club_ownership(
			origin: OriginFor<T>,
			club: ClubId,
			old_owner: Option<T::AccountId>,
			new_owner: T::AccountId,
		) -> DispatchResult {
			ensure_root(origin)?;
			match (ClubOwners::<T>::get(club), old_owner) {
				(Some(owner), Some(old_owner)) if owner == old_owner => (),
				(_, None) => (), /* Skip check if no data. Also keep in mind, that `None, None` */
				// is a valid situation.
				_ => return Err(Error::<T>::NonExistentOwner.into()),
			};
			Self::deposit_event(Event::ClubOwnerChanged {
				club_id: club,
				old_owner: ClubOwners::<T>::get(club),
				new_owner: new_owner.clone(),
			});
			Self::set_club_owner(new_owner, club)?;
			Ok(())
		}

		#[pallet::call_index(2)]
		#[pallet::weight(T::DbWeight::get().reads_writes(2, 1) + Weight::from(14_000))]
		/// Add user to club.
		///
		/// This is privileged operation to be run by `root`.
		///
		/// This function does not immediately start the billing
		/// cycles. Instead, a starting method should be hooked into
		/// the emitted event: [`Event::UserAdded`]. See
		/// [`Self::bill_club_users`] for the rationale.
		pub fn add_user_to_club(
			origin: OriginFor<T>,
			club_id: ClubId,
			user: T::AccountId,
			initial_subscription_payment: BalanceOf<T>,
		) -> DispatchResult {
			let account = ensure_signed(origin)?;
			match ClubOwners::<T>::get(club_id) {
				Some(owner) if owner == account => return Err(Error::<T>::Unauthorised.into()),
				_ => (),
			};
			let initial_cycles = Clubs::<T>::get(club_id)
				.ok_or(Error::<T>::NonExistentClub)?
				.cycles(initial_subscription_payment);
			Clubs::<T>::mutate(club_id, |club_opt| {
				if initial_cycles.0 == 0u32 {
					club_opt
						.as_mut()
						.map(|club: &mut Club<T>| {
							if club
								.users_pending_renewal
								.try_insert(user, initial_cycles.1)
								.map_err(|_| Error::<T>::TooManyAccountsPendingRenewal)?
								.is_some()
							{
								Err(Error::<T>::UserCollision.into())
							} else {
								Self::deposit_event(Event::UserAdded {
									added_user: account.clone(),
									club_id,
								});
								Ok(())
							}
						})
						.ok_or(Error::<T>::NonExistentClub)?
				} else {
					club_opt
						.as_mut()
						.map(|club: &mut Club<T>| {
							if club.users.contains_key(&account) {
								Err(Error::<T>::UserCollision.into())
							} else {
								club.users
									.try_insert(user, initial_cycles)
									.map_err(|_| Error::<T>::TooManyActiveUsers)?;
								Self::deposit_event(Event::UserAdded {
									added_user: account.clone(),
									club_id,
								});
								Ok(())
							}
						})
						.ok_or(Error::<T>::NonExistentClub)?
				}
			})
		}

		#[pallet::call_index(3)]
		#[pallet::weight(T::DbWeight::get().reads_writes(0, 1) + Weight::from(20_000))]
		/// Dispatchable to be attached to a block processing event,
		/// which must be run for each club, during its billing cycle.
		///
		/// This is a privileged operation to be run by root, whenever
		/// a new club has been added.
		///
		/// This function is deliberately unused in the pallet,
		/// `pallet_scheduler` should be called from within the
		/// runtime to call this dispatchable. The main reason being,
		/// that in order to make an efficeint impleemntation, one
		/// needs to know the block time, which can become
		/// recursive. One solution is to hook into the
		/// [`Event::ClubCreated`], and attach a starter method which
		/// determines when to check which clubs.
		///
		/// As this is a pallet, (read library), not all functionality
		/// must be implemented anyway.
		pub fn bill_club_users(origin: OriginFor<T>, club_id: ClubId) -> DispatchResult {
			ensure_root(origin)?;
			Clubs::<T>::mutate(club_id, |club_opt| {
				club_opt
					.as_mut()
					.map(|club| {
						let mut working_copy = BoundedBTreeMap::default();
						core::mem::swap(&mut club.users, &mut working_copy);
						let (mut live, expired): (BTreeMap<_, _>, BTreeMap<_, _>) = working_copy
							.into_iter()
							.partition(|(_, (subscription, _))| *subscription > 1);
						for (_, (ref mut s, _)) in live.iter_mut() {
							*s -= 1;
						}
						let mut live: BoundedBTreeMap<_, _, _> = live.try_into().unwrap();
						core::mem::swap(&mut club.users, &mut live);
						for (key, (_, credit)) in expired {
							// TODO: report errror
							match club.users_pending_renewal.try_insert(key.clone(), credit) {
								Err(_) => return Err(Error::<T>::TooManyAccountsPendingRenewal),
								Ok(None) => Self::deposit_event(Event::SubscriptionExpired {
									club_id,
									for_whom: key,
								}),
								Ok(Some(_)) => panic!(
									"A user is both active and expired. This is a logic error"
								),
							}
						}
						Ok(())
					})
					.ok_or(Error::<T>::NonExistentClub)
			})?
			.map_err(Into::into)
		}

		#[pallet::call_index(4)]
		#[pallet::weight(T::DbWeight::get().reads_writes(0, 1) + Weight::from(3_000))]
		/// Dispatchable to renew subscription of any user that is
		/// either currently active or is pending renewal.
		pub fn renew_subscription(
			origin: OriginFor<T>,
			club: ClubId,
			subscription_payment: BalanceOf<T>,
		) -> DispatchResult {
			let account = ensure_signed(origin.clone())?;
			Clubs::<T>::mutate(club, |club_opt| {
				club_opt
					.as_mut()
					.map(|club| renew_subscription(club, &account, subscription_payment))
					.ok_or(Error::<T>::NonExistentClub)?
			})?;
			Ok(())
		}

		#[pallet::call_index(5)]
		#[pallet::weight(T::DbWeight::get().reads_writes(0, 1) + Weight::from(5_000))]
		/// Setter Dispatchable for club owners to set annual expenses
		pub fn set_annual_expenses(
			origin: OriginFor<T>,
			club_id: ClubId,
			annual_expenses: BalanceOf<T>,
		) -> DispatchResult {
			let account = ensure_signed(origin)?;
			let owner = ClubOwners::<T>::get(club_id).ok_or(Error::<T>::NonExistentClub)?;
			if owner == account {
				Clubs::<T>::mutate(club_id, |club_opt| {
					club_opt.as_mut().map(|club| {
						club.annual_expenses = annual_expenses;
						Self::deposit_event(Event::AnnualExpensesChanged {
							club_id,
							annual_expenses,
						})
					})
				})
				.ok_or(Error::<T>::NonExistentClub)
			} else {
				Err(Error::<T>::Unauthorised)
			}
			.map_err(Into::into)
		}
	}

	impl<T: Config> Pallet<T> {
		/// Add a club, if the club didn't exist before.
		///
		/// # Errors
		/// - If club already exists
		fn add_club(new_club_id: ClubId, club: Club<T>) -> Result<(), Error<T>> {
			if Clubs::<T>::contains_key(new_club_id) {
				return Err(Error::ClubNameCollision)
			}
			Clubs::<T>::insert(new_club_id, Some(club));
			Ok(())
		}

		/// Set the club `owner` of a particular `club_id`
		/// Privileged operation to be executed by `root`.
		///
		/// # Errors
		/// - if club doesn't exist
		fn set_club_owner(owner: T::AccountId, club_id: u32) -> Result<(), Error<T>> {
			if Clubs::<T>::contains_key(club_id) {
				ClubOwners::<T>::set(club_id, Some(owner));
				Ok(())
			} else {
				Err(Error::NonExistentClub)
			}
		}
	}

	// #[pallet::event]
	// pub enum Event<T: Config> {}
}
