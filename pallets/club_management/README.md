## Substrate Assessment

Write a pallet for club member management

- [x] two roles in the club, root can create a new club, club owner can add someone to club
- [x] need to pay some token to create a new club, the club owner can be transferred, club owner can set the annual expense for club membership
- [x] account needs to pay a token to be a member based on annual expenses, max is 100 years
- [x] membership will be expired, and need renewal

This code is a clone of the Substrate node developer template (along with everything), this pallet is the interesting bit. 

## Extra-functional reqs. 

- [x] Supporting data unit tests
- [ ] functional unit tests
- [x] weights (manual)
- [ ] Benches
